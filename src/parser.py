#!/usr/bin/env python3.8
# coding: utf-8
import os
import argparse
import json



# Définition des constantes du modèle
DATA_DIR        = 'data/'
OUT_DIR         = 'out/'

EPOCHS          = 50

LR              = .001
HIDDEN_DIM      = 128
NUM_GRU         = 2
DROPOUT         = 0.2

NUM_MFCC        = 13
MFCC_LEN        = 1000
NUM_FRAME_SAMPLE= 50

BATCH_SIZE      = 128

PATIENCE        = 5



def parse_args():
    '''
    Parse les arguments entrés par l'utilisateur au lancement du programme

    Returns
    ----------
    Namespace(
        data_dir: str,
        out_dir: str,
        learning_rate: float,
        epochs: int,
        hidden_dim: int,
        num_lstm: int,
        batch_size: int,
        patience: int,
        num_frame_sample: int,
        verbose: bool,
        clear_logs: bool
    )
        Le namespace correspondant aux arguments parsés
    '''
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-dd", "--data_dir",
        type = str,
        default = DATA_DIR,
        help = f'Name of the data directory (default: {DATA_DIR})'
    )
    parser.add_argument(
        "-od", "--out_dir",
        type = str,
        default = OUT_DIR,
        help = f'Name of the data directory (default: {OUT_DIR})'
    )

    parser.add_argument(
        '-e', '--epochs',
        type = int,
        default = EPOCHS,
        help = f'Maximum number of iterations used for the learning process (default: {EPOCHS})'
    )

    parser.add_argument(
        '-lr', '--learning_rate',
        type = float,
        default = LR,
        help = f'Learning rate of each unit composing the model (default: {LR})'
    )
    parser.add_argument(
        '-hd', '--hidden_dim',
        type = int,
        default = HIDDEN_DIM,
        help = f'Number of logic units per hidden layer (default: {HIDDEN_DIM})'
    )
    parser.add_argument(
        '-ngru', '--num_gru',
        type = int,
        default = NUM_GRU,
        help = f'Number of GRU units (default: {NUM_GRU})'
    )
    parser.add_argument(
        '-d', '--dropout',
        type = float,
        default = DROPOUT,
        help = f'Dropout rate of the GRU (default: {DROPOUT})'
    )

    parser.add_argument(
        '-nmfcc', '--num_mfcc',
        type = int,
        default = NUM_MFCC,
        help = f'Number of main components of the MFCC (default: {NUM_MFCC})'
    )
    parser.add_argument(
        '-mfccl', '--mfcc_len',
        type = int,
        default = MFCC_LEN,
        help = f'Maximum size of a frame in the MFCC splitting process (default: {MFCC_LEN} to cut the audio into {MFCC_LEN * 10 / 1000}s subsamples)'
    )
    parser.add_argument(
        '-nfs', '--num_frame_sample',
        type = int,
        default = NUM_FRAME_SAMPLE,
        help = f'Number of samples / lines a single frame should contain (default: {NUM_FRAME_SAMPLE})'
    )    

    parser.add_argument(
        '-bs', '--batch_size',
        type = int,
        default = BATCH_SIZE,
        help = f'Number of processed sequences per iteration (default: {BATCH_SIZE})'
    )

    parser.add_argument(
        '-p', '--patience',
        type = int,
        default = PATIENCE,
        help = f'Number of epochs to wait before stopping the learning process (default: {PATIENCE})'
    )

    parser.add_argument(
        '-v', '--verbose',
        action = 'store_true',
        help = 'Starts the script in explicit mode (default: false)'    
    )
    parser.add_argument(
        '-cl', '--clear_logs',
        action='store_true',
        help = 'Clear the logs linked to `dataset_id` (default: false)'
    )
    
    args = parser.parse_args()

    # On s'assure qu'un '/' soit apposé pour les chemins vers les dossiers de 
    # données et de sortie.
    if args.data_dir[-1] != '/':
        args.data_dir += '/'
    if args.out_dir[-1] != '/':
        args.out_dir += '/'
    
    return args

def corpus_from_json(data_dir):
    '''
    Récupère la liste des caractéristiques des fichiers audio constituant 
    chaque corpus, sous forme d'un dictionnaire de données.
    N.B.:
    Lance `fix_json()` si cette fonction ne trouve pas de fichiers 
    `*-fixed.json` dans le dossier de données.

    Paramètres
    ----------
    data_dir: str
        Dossier contenant les fichiers `*-fixed.json`
    
    Retourne
    ----------
    metadata: dict( str: list({
        "audio_file_path": str,
        "duration": float,
        "id": str,
        "is_hotword": int (0|1),
        "worker_id": str
    }) )
        Dictionnaire des informations de chacun des corpus de données. 
    '''
    metadata = { 'train': [], 'test': [], 'dev': [] }

    # On commence par vérifier que toutes les métadonnées corrigées 
    # `*-fixed.json` soient présentes dans le dossier `data_dir`
    for dtype in metadata:
        if not os.path.isfile(f'{data_dir}{dtype}-fixed.json'):
            fix_json(data_dir)

    for dtype in metadata:
        with open(f'{data_dir}{dtype}-fixed.json') as json_file:
            metadata[dtype] = json.load(json_file)
    
    return metadata


def fix_json(data_dir):
    '''
    Récupère l'ensemble des fichiers `*.json` du corpus de données et corrige 
    celles dont la durée du fichier audio n'est pas définie (champ `duration` 
    à `null`), dans des fichiers `*-fixed.json`
    
    Paramètres
    ----------
    data_dir: str
        Dossier contenant les fichiers `*.json`
    '''
    # On commence par récupérer les durées de chaque fichiers audio dans le  
    # fichier `duration_all_audio_files` contenu dans le dossier de données
    durations = {}
    with open(f'{data_dir}duration_all_audio_files') as file:
        for line in file:
            buffer = line.split('\n')[0].split()
            durations[buffer[0]] = float(buffer[1])

    # On parse sous forme de dictionnaire les informations des différents 
    # corpus stocké sous forme de fichiers .json, et complète les entrées dont 
    # la durée du fichier audio n'est pas renseignée, via le dictionnaire 
    # `durations` ci-dessus. 
    for corpus_name in [ 'dev', 'test', 'train' ]:
        corpus = []
        with open(f'{data_dir}{corpus_name}.json') as json_file:
            corpus = json.load(json_file)
        
        for audio_id, audio_info in enumerate(corpus):
            if audio_info['duration'] is None:
                corpus[audio_id]['duration'] = durations[
                    f'{audio_info["audio_file_path"].replace("audio_files", ".")}'
                ]
        
        # On enregistre les informations corrigées des différents corpus
        with open(f'{data_dir}{corpus_name}-fixed.json', 'w') as json_file:
            json.dump(corpus, json_file, indent = 4, sort_keys = True)


def parse_output(fname, otype):
    '''
    Récupère les données d'un fichier contenant l'évolution de la loss d'un modèle

    Paramètres
    ----------
    fname: str
        Nom du fichier à parser
    otype: str {'loss', 'eval'}
        Type de sortie à parser
    
    Retourne
    ----------
    data: list(float) | { 'n-mfcc': list(int), 'accuracy': list(float) }
        Listes des coûts calculés au fil de l'entraînement du modèle
            OU
        Dictionnaire contenant les informations extraites du fichier 
        d'évaluation.        
    '''
    file = open(fname, 'r')
    first_line = True

    data = [] if otype == 'loss' else { 'n-mfcc': [], 'accuracy': [], 'epochs': [] }

    for line in file:
        if first_line: 
            first_line = False
        else:
            buffer = line.split('\n')[0]
            if otype == 'loss':
                data += [ float(buffer) ]
            elif otype == 'eval':
                buffer = line.split('\t')
                data['n-mfcc'] += [ int(buffer[0]) ]
                data['epochs'] += [ int(buffer[1]) ]
                data['accuracy'] += [ float(buffer[2]) ]

    file.close()
    return data


if __name__ == '__main__':
    args = parse_args()
    print(args)
    print(corpus_from_json(args.data_dir))
     