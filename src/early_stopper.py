#!/usr/bin/env python3.8
# coding: utf-8
import torch
import numpy
from logger import Logger



class EarlyStopper:
    '''
    Classe en charge de gérer l'arrêt prématuré de la phase d'apprentissage.
    Cet arrêt est sensé se déclencher dès lors que le score du modèle sur le 
    jeu de validation ne s'améliore pas sur un certain nombre d'époques 
    (patience).

    Attributs
    ----------
    patience: int
        Nombre d'époques maximum ne validant pas une amélioration du critère à
        optimiser (en l'occurence ici, minimiser la fonction de coût).
    delta: float
        Différentiel minimal qualifiant l'amélioration de la fonction de coût

    Crédits
    ----------
    Merci @Bjarten pour son tutoriel sur l'early stopping sous pytorch :
    https://github.com/Bjarten/early-stopping-pytorch/blob/master/pytorchtools.py
    '''
    def __init__(self, patience, delta):
        self.patience = patience
        self.delta = delta
        self.counter = 0
        self.min_loss = numpy.Inf
        self.accuracy = numpy.Inf
        self.early_stop = False
    

    def __call__(self, loss, model, epoch, accuracy):
        if self.min_loss == numpy.Inf:
            # L'instance est vierge de tout configuration optimale, on 
            # enregistre la première venue.
            self.update_checkpoint(loss, model, epoch, accuracy)
        elif loss - self.min_loss > self.delta:
            # La configuration actuelle du modèle ne minimise pas le critère de 
            # coût.
            self.counter += 1
            Logger.warning(f'Early stop status: {self.counter:>3} / {self.patience:<3} [ epoch: {epoch:>3}, acc.: {accuracy:>7.4f}%, loss: {loss:>9.5f} ]')
            if self.counter >= self.patience:
                self.early_stop = True
        else:
            # On vient de trouver un réseau minimisant la fonction objectif. Il
            # s'agit maintenant de mettre à jour le checkpoint.
            self.update_checkpoint(loss, model, epoch, accuracy)
            self.counter = 0


    def update_checkpoint(self, loss, model, epoch, accuracy):
        '''
        Création d'un point de sauvegarde du modèle.
        Cette fonction est appelée dans les situations où le modèle dispose 
        d'une configuration minimisant la fonction de coût.

        Paramètres
        ----------
        loss: float
            Valeur de la fonction de coût
        model: Net
            Réseau à sauvegarder
        '''
        Logger.stress(f'Validation loss decreased!   [ epoch: {epoch:>3}, acc.: {accuracy:>7.3f}%, from: {self.min_loss:>9.5f}, to: {loss:>9.5f} ]')
        Logger.subinfo('Saving model..')
        torch.save(model.state_dict(), f'cpt/checkpoint-{Logger.NUM_MFCC}.pt')
        self.min_loss = loss
        self.accuracy = accuracy
