#!/usr/bin/env python3.8
# coding: utf-8
import os



EPOCHS = 100
PATIENCE = 5
NUM_MFCC = [ 30 ]


print('Benchmarking model..\n')

for n in NUM_MFCC:
    print(f'⚲  #MFCCs: {n}') 
    for run in range(6):
        print(f'   ›  run: {run}')
        # Entraînement du modèle
        os.system(f'python3 src/net.py -nmfcc {n} -e {EPOCHS} -v')
        # On met à jour les graphiques de suivi
        os.system(f'python3 src/plotter.py -f out/loss.dat -dt loss')
        os.system(f'python3 src/plotter.py -f out/eval.dat -dt eval')

print('Done!')