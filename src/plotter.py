from parser import parse_output
import numpy
import plotly.graph_objects as go
import argparse



STD_LAYOUT = {
    'title': {
        'y':0.95,
        'x':0.5,
        'xanchor': 'center',
        'yanchor': 'top',
        'font': {
            'family': 'Ubuntu, bold',
            'size': 20,
        },
    },
    'xaxis': {
        'gridcolor': 'white',
    },
    'yaxis': {
        'gridcolor': 'white',
    },
    'font': {
        'family': 'Ubuntu',
    },
    'showlegend': False,
    'paper_bgcolor': 'rgb(233,233,233)',
    'plot_bgcolor': 'rgb(233,233,233)',
}



def loss_to_html(fname):
    '''
    Trace la courbe d'évolution de la fonction de coût au fil des itérations, 
    dans un fichier au format HTML dans le dossier éponyme.

    Paramètres
    ----------
    fname: str
        Nom du fichier contenant les données brutes
    '''
    loss = parse_output(fname, 'loss')
    it = list(range(len(loss)))

    fig = go.Figure({
        'data': [
            go.Scatter({
                'x': it,
                'y': loss,
                'name': f'loss',
            })
        ]
    })

    layout = STD_LAYOUT
    layout['title']['text'] = 'Loss evolution through iterations'
    layout['xaxis']['title'] = 'Iteration'
    layout['xaxis']['zeroline'] = True
    layout['yaxis']['title'] = 'Loss'
    layout['yaxis']['zeroline'] = True
    fig.update_layout(layout)

    fig.write_html(f'html/loss.html')


def eval_to_html(fname):
    '''
    Trace la répartition des performances du modèle sous forme de violons dans
    un fichier au format HTML, dans le dossier éponyme.

    Paramètres
    ----------
    fname: str
        Nom du fichier contenant les données brutes
    '''
    data = parse_output(fname, 'eval')
    num_mfccs = list(set(data['n-mfcc']))
    num_mfccs.sort()

    fig = go.Figure({
        'data': [
            go.Violin({
                'x0': f'#MFCC {num_mfcc}',
                'y': [ accuracy for idx, accuracy in enumerate(data['accuracy']) if data['n-mfcc'][idx] == num_mfcc ],
                'name': f'mfcc-{num_mfcc}',
                'box_visible': True,
                'meanline_visible': True,
                'points': 'all', # show all points
                'jitter': 0.05,  # add some jitter on points for better visibility
                'scalemode': 'count'
            }) for num_mfcc in num_mfccs
        ]
    })

    layout = STD_LAYOUT
    layout['title']['text'] = 'Accuracy distribution given the MFCC size'
    layout['xaxis']['zeroline'] = False
    layout['yaxis']['title'] = 'Accuracy (in %) distribution'
    layout['yaxis']['zeroline'] = False
    fig.update_layout(layout)

    fig.write_html('html/eval.html')



if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument(
        "-f", "--file",
        type = str,
        required = True,
        help = 'Name of the data file to plot.'
    )
    ap.add_argument(
        "-dt", "--data_type",
        type = str,
        choices = ['loss', 'eval'],
        required = True,
        help = 'Type of data to plot.'
    )
    args = ap.parse_args()

    if args.data_type == 'loss':
        loss_to_html(args.file)
    else:
        eval_to_html(args.file)
