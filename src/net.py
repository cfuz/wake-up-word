#!/usr/bin/env python3.8
# coding: utf-8
import torch
from dataset import Dataset
from early_stopper import EarlyStopper
from logger import Logger
import parser



class Net(torch.nn.Module):
    '''
    Modèle de l'application de détection de mot d'éveil.
    
    Attributs
    ----------
    (static) DEVICE: torch.device()
        Addresse de la ressource allouée pour le chargement et le traitement 
        des données d'entrées.

    lr: float
        Taux d'apprentissage du réseau de neurones.
    hidden_dim: int
        Nombre d'unités cachées composant une cellule GRU.
    gru: torch.nn.GRU
        Réseau de neurone récurrent traitant les séquences fournies par le 
        DataLoader.
    fc1: torch.nn.Linear
        Première couche linéaire transformant les données de sortie du réseau
        récurrent.
    relu: torch.nn.ReLU()
        Filtre de régression linéaire appliqué aux résultats de la première 
        couche linéaire.
    fc2: torch.nn.Linear
        Dernière couche linéaire de la propagation avant du signal d'entrée.
    optimizer: torch.optim.Adam
        Système de gestion de la rétropropagation des erreurs du modèle. Adam 
        permet notamment d'ajuster de façon dynamique la valeur du pas 
        d'apprentissage, pour accélérer le processus.
    criterion: torch.nn.BCEWithLogitsLoss
        Fonction objectif évaluant les écarts de prédictions aux étiquettes du 
        jeu de données. BCEWithLogitsLoss permet d'évaluer les résultats 
        sortant (logits) d'une couche de réseau de neurones.
    '''
    DEVICE = None



    def __init__(
        self,
        seq_len,
        lr,
        input_size, 
        hidden_dim,
        output_dim,
        num_layers,
        dropout
    ):
        super(Net, self).__init__()
        
        # Pour réduire le temps de résolution, on commence d'abord par vérifier
        # la disponibilité d'un GPU.
        if Net.DEVICE is None:
            Net.DEVICE = ( torch.device('cuda:1') if torch.cuda.is_available()
                else torch.device('cpu') )
        
        self.lr = lr
        self.hidden_dim = hidden_dim

        self.gru = torch.nn.GRU(
            input_size, 
            hidden_dim,
            num_layers,
            dropout = dropout,
            batch_first = True, 
        )
        self.fc1 = torch.nn.Linear( hidden_dim, output_dim )
        self.fc2 = torch.nn.Linear( seq_len * output_dim, output_dim )
        self.relu = torch.nn.ReLU()

        # Fonction de coût associée au modèle : permet d'estimer les 
        # prédictions du réseau à la réalité du terrain selon une certaine
        # fonction à minimiser.
        self.optimizer = torch.optim.Adam(self.parameters(), lr = self.lr)
        # self.optimizer = torch.optim.SGD(self.parameters(), lr = self.lr, momentum = 0.9)
        
        # Optimiseur du modèle en charge de la gestion de la rétropropagation 
        # des erreurs: Adam permet entre-autre d'ajuster la valeur du pas 
        # d'apprentissage.
        self.criterion = torch.nn.BCEWithLogitsLoss()
    
    
    def forward(self, x):
        out, _hidden_state = self.gru(x)
        out = self.fc1(out)
        out = self.relu(out)
        return self.fc2(out.view(out.shape[0], -1)).view(-1)

    
    def fire(
        self, 
        train_loader, 
        valid_loader, 
        epochs,
        patience,
    ):
        '''
        Lance la procédure d'apprentissage du réseau.

        Paramètres
        ----------
        train_loader: torch.data.DataLoader
            Liste des protéines composant le jeu d'entraînement
        epochs: int
            Nombre maximum d'itération dans le processus d'apprentissage
        '''
        Logger.info('Learning process fired!')
        self.to(Net.DEVICE)
        
        epoch = 0
        early_stopper = EarlyStopper(patience, delta = 1.0e-8)

        while epoch < epochs:
            avg_loss = self.train_net(
                train_loader,
                epoch
            )
            Logger.stress(f'{f"Avg. training loss: {avg_loss:.5f}":<28} [ epoch: {epoch:>3} ]')
            
            avg_loss, accuracy = Net.EVAL(
                self,
                valid_loader,  
                epoch
            )
            Logger.stress(f'{f"Accuracy:":<28} [ epoch: {epoch:>3}, avg. loss: {avg_loss:>9.5f} ]')
            Logger.subsubinfo(f'True positive : {accuracy["true-pos"]}')
            Logger.subsubinfo(f'True negative : {accuracy["true-neg"]}')
            Logger.subsubinfo(f'False positive: {accuracy["false-pos"]}')
            Logger.subsubinfo(f'False negative: {accuracy["false-neg"]}')
            Logger.subsubinfo(f'Overall       : {accuracy["overall"]:.4f}%')

            if abs(accuracy['overall'] - 89.04) < 1.0e-2:
                Logger.error('Bottleneck spotted! Cancelling learning process…')
                break

            '''
            Vérification de la valeur de la moyenne de la fonction de coût sur 
            le jeu de validation. Si cette quantité a diminué, on créé une 
            sauvegarde/checkpoint de la configuration du modèle.
            '''
            early_stopper(avg_loss, self, epoch, accuracy['overall'])
            if early_stopper.early_stop:
                Logger.update('Early stopping!')
                break

            epoch += 1
        
        '''
        Sorti de la boucle d'apprentissage on charge la configuration 
        minimisant la fonction de coût sur le jeu de validation via le 
        checkpoint stocké par l'`early_stopper`.
        '''
        self.load_state_dict(torch.load(f'cpt/checkpoint-{Logger.NUM_MFCC}.pt'))
        Logger.info(f'{"Status":<28} [ epochs: {epoch:>3}, acc.: {early_stopper.accuracy:>7.3f}, min. loss: {early_stopper.min_loss:>9.5f} ]')

        return epoch


    def train_net(self, train_loader, epoch):
        '''
        Entraîne le modèle

        Paramètres
        ----------
        train_loader: DataLoader
            Jeu d'entraînement
        epoch: int
            Date à laquelle est réalisé la phase d'entraînement (utile 
            simplement pour les logs)
        
        Retourne
        ----------
        La valeur moyenne de fonction de coût émergeant de l'entraînement
        '''
        Logger.info('Training model')

        # Configure le modèle pour l'entraînement
        self.train()
        avg_train_loss = 0.0
        avg_loss = 0.0
        count = 0

        for bidx, (idx, inputs, labels) in enumerate(train_loader):
            # On charge le batch sur le GPU si possible
            inputs, labels = inputs.to(Net.DEVICE), labels.to(Net.DEVICE)

            # Vide les gradients de toutes les variables optimisées
            self.optimizer.zero_grad()
            
            # Génération des prédictions par propagation avant des données 
            # du jeu.
            pred = self(inputs)
            
            # Calcul du taux d'erreur lié aux prédictions ainsi réalisées 
            # par le modèle.
            loss = self.criterion(pred, labels.float())
            # Rétropropagation des erreurs de prédicitions en regard du 
            # criterion définit plus haut.
            loss.backward()
            # On réalise une étape d'optimisation pour mettre à jour les 
            # paramètres du réseau. 
            self.optimizer.step()

            # On trace la loss pour calculer la moyenne des valeurs 
            # renvoyées par cette fonction sur `periodic_checks` mini-batchs.
            avg_loss += loss.item()
            avg_train_loss += loss.item()
            if bidx % 25 == 24:
                avg_loss = float(avg_loss) / 25.0
                Logger.update(f'{"Learning status..":<28} [ epoch: {epoch:>3}, batch id: {bidx:>4}, avg. loss: {avg_loss:>9.5f} ]')
                Logger.log_loss(loss)
                avg_loss = 0.0
            
            count += 1

        return avg_train_loss / float(count)


    @staticmethod
    def EVAL(net, loader, epoch):
        '''
        Évalue la robustesse de la configuration actuelle sur le jeu de 
        validation.
        N.B. : 
        La précision est calculé sur la prédiction du modèle sur un audio 
        complet. Ainsi il s'agira de déterminer si oui ou non notre système est
        capable de fournir un jugement unanime et juste sur l'ensemble des 
        fenêtres découpant un audio.

        Paramètres
        ----------
        loader: DataLoader
            Jeu de validation
        epoch: int
            Date à laquelle est réalisé la phase d'entraînement (utile 
            simplement pour les logs)

        Retourne
        ----------
        avg_loss: float
            Valeur moyenne de la fonction de coût sur le jeu de données
        accuracy: {
            'overall': float,
            'true-pos': int,
            'true-neg': int,
            'false-pos': int,
            'false-neg': int, 
        }
            Précision du modèle sur les audios du corpus
        '''
        Logger.info('Evaluating model')
        # Configure le réseau pour évaluation de ses paramètres
        net.eval()

        avg_loss = 0.0
        count = 0

        num_audio = float(len(loader.dataset.metadata))
        accuracy = {
            'overall': num_audio,
            'true-pos': 0,
            'true-neg': 0,
            'false-pos': 0,
            'false-neg': 0, 
        }

        with torch.no_grad():
            for bidx, (idx, inputs, labels) in enumerate(loader):
                # On charge le batch sur le GPU si possible
                inputs, labels = inputs.to(Net.DEVICE), labels.to(Net.DEVICE)

                pred = net(inputs)
                loss = net.criterion(pred, labels)

                avg_loss += loss.item()

                # On récupère la liste des indices correspondant aux mauvaises 
                # prédictions.
                pred[pred <= 0.0] = 0.0
                pred[pred > 0.0] = 1.0
                wrong_pred = pred != labels

                audio_idx = loader.dataset.to_id[idx[0].item()]
                badly_predicted = False
                previous_pred = pred[0].item()

                for lidx, label in enumerate(labels):
                    chunk_audio_idx = loader.dataset.to_id[idx[lidx].item()]
                    if audio_idx != chunk_audio_idx:
                        audio_idx = chunk_audio_idx
                        if not badly_predicted:
                            if previous_pred == 1.0:
                                accuracy['true-pos'] += 1
                            else: 
                                accuracy['true-neg'] += 1
                        else:
                            badly_predicted = False
                    
                    if not badly_predicted:
                        if wrong_pred[lidx]:
                            accuracy['overall'] -= 1.0
                            if pred[lidx] == 1.0 and label == 0.0:
                                accuracy['false-pos'] += 1
                            if pred[lidx] == 0.0 and label == 1.0:
                                accuracy['false-neg'] += 1
                            badly_predicted = True
                        previous_pred = pred[lidx]
                    else:
                        continue
                # accuracy -= wrong_idx.sum().item()                        

                count += 1
        
        avg_loss /= float(count)
        accuracy['overall'] = (accuracy['overall'] / num_audio) * 100.0
        return avg_loss, accuracy



if __name__ == "__main__":
    args = parser.parse_args()

    # On active le Logger si nécessaire
    Logger.VERBOSE = args.verbose
    Logger.NUM_MFCC = args.num_mfcc

    # Vide le fichier des logs du dossier `out/` si nécessaire
    if args.clear_logs:
        Logger.clear_logs()

    Logger.clear_loss_logs()

    metadata = parser.corpus_from_json(args.data_dir)

    # Définition des paramètres du mini-batch
    setup = {
        'batch_size': args.batch_size,
        'shuffle': True,
        'num_workers': 8
    }

    train_loader = torch.utils.data.DataLoader(
        Dataset(
            'train',
            metadata['train'],
            args.data_dir,
            args.mfcc_len,
            args.num_mfcc,
            args.num_frame_sample
        ),
        **setup
    )

    setup['shuffle'] = False
    valid_loader = torch.utils.data.DataLoader(
        Dataset('dev', metadata['dev']),
        **setup
    )
    test_loader = torch.utils.data.DataLoader(
        Dataset('test', metadata['test']), 
        **setup
    )
    
    seq_len = train_loader.dataset.inputs.shape[1]
    input_size = train_loader.dataset.inputs.shape[2]

    num_epochs = 0
    while num_epochs == 0:
        net = Net(
            seq_len,
            args.learning_rate,
            input_size,
            args.hidden_dim,
            Dataset.OUT_SIZE, # Les prédictions du modèle sont binaires
            args.num_gru,
            args.dropout
        )

        num_epochs = net.fire(
            train_loader, 
            valid_loader, 
            args.epochs,
            args.patience
        )

        if num_epochs != 0:
            Logger.success( 'Done!' )
        else:
            Logger.info( 'Restarting training phase…' )
            Logger.clear_loss_logs()
        
        if torch.cuda.is_available():
            torch.cuda.empty_cache()
    
    Logger.info('Evaluating model on test dataset')
    avg_loss, accuracy = Net.EVAL(net, test_loader, num_epochs)
    Logger.stress(f'{f"Accuracy:":<28} [ epoch: {num_epochs:>3}, avg. loss: {avg_loss:>9.5f} ]')
    Logger.subsubinfo(f'True positive : {accuracy["true-pos"]}')
    Logger.subsubinfo(f'True negative : {accuracy["true-neg"]}')
    Logger.subsubinfo(f'False positive: {accuracy["false-pos"]}')
    Logger.subsubinfo(f'False negative: {accuracy["false-neg"]}')
    Logger.subsubinfo(f'Overall       : {accuracy["overall"]:.4f}%')

    Logger.info('Logging evaluation score')
    Logger.log_eval(num_epochs, accuracy['overall'])
    Logger.success( 'Done!' )
