#!/usr/bin/env python3.8
# coding: utf-8



class Logger:
    '''
    Classe chargée de logger les informations qui lui sont transmises en 
    fonction de la verbosité requise.

    Attributs
    ----------
    VERBOSE: bool
        Booléen définissant la verbosité du logger
    '''
    VERBOSE = False
    NUM_MFCC = None

    @staticmethod
    def info(message):
        if Logger.VERBOSE:
            print(f"➜  {message}")
    
    @staticmethod
    def subinfo(message):
        if Logger.VERBOSE:
            print(f"   {message}")

    @staticmethod
    def subsubinfo(message):
        if Logger.VERBOSE:
            print(f"      {message}")

    @staticmethod
    def success(message):
        if Logger.VERBOSE:
            print(f"✔  {message}")
    
    @staticmethod
    def update(message):
        if Logger.VERBOSE:
            print(f"ϟ  {message}")
    
    @staticmethod
    def warning(message):
        if Logger.VERBOSE:
            print(f"☢  {message}")
    
    @staticmethod
    def error(message):
        if Logger.VERBOSE:
            print(f"✘  {message}")
    
    @staticmethod
    def wait(message):
        if Logger.VERBOSE:
            print(f"⚙  {message}…")

    @staticmethod
    def stress(message):
        if Logger.VERBOSE:
            print(f"⚝  {message}")


    @staticmethod
    def clear_logs():
        file = open( f'out/eval.dat', 'w' )
        file.write(f'{"#MFCC"}\t{"epochs":<6}\t{"accuracy":<9}\n')
        file.close()
        Logger.clear_loss_logs()

    @staticmethod
    def clear_loss_logs():
        file = open( f'out/loss.dat', 'w' )
        file.write(f'{"loss":<9}\n')
        file.close()

    @staticmethod
    def log_eval(epochs, accuracy):
        file = open(f'out/eval.dat', 'a+')
        file.write(f'{Logger.NUM_MFCC:<5}\t{epochs:<6}\t{accuracy:<9.5f}\n')
        file.close()
    
    
    @staticmethod
    def log_loss(loss):
        file = open(f'out/loss.dat', 'a+')
        file.write(f'{loss:<9.5f}\n')
        file.close()