#!/usr/bin/env python3.8
# coding: utf-8
import torch
import torchaudio
import os
import pickle
from torch.utils import data
from logger import Logger



class Dataset(torch.utils.data.Dataset):
    '''
    Structure spécifique au stockage des données du jeu d'audios 'Hey Snips!'. 
    Pour son utilisation dans le cadre de l'entraînement d'un modèle :
    https://stanford.edu/~shervine/blog/pytorch-how-to-generate-data-parallel

    Attributs
    ----------
    (static) DATA_DIR: str
        Nom du dossier contenant les données
    (static) MFCC_LEN: int
        Longueur maximale d'une décomposition fréquentielle /  taille d'une fenêtre
    (static) N_FFT: float
        Nombre de tranformées de Fourier réaliser
    (static) NUM_MFCC: int
        Nombre de composantes constituant les MFCCs
    (static) MFCC_PARAMS: {
        "channel": int,
        "dither": float,
        "window_type": str,
        "frame_length": int,
        "frame_shift": int,
        "remove_dc_offset": bool,
        "round_to_power_of_two": bool,
        "sample_frequency": int,
        "num_ceps": int, # Nombre de composantes principales à retenir dans la décomposition
        "num_mel_bins": int,
        'snip_edges': bool,
    } | None
        Paramètres de la MFCC de la bibliothèque Kaldi
    
    metadata: list({
        "audio_file_path": str,
        "duration": float,
        "id": str,
        "is_hotword": int (0|1),
        "worker_id": str
    })
        Liste des métadonnées composant le corpus
    inputs: torch.tensor( ?, MFCC_LEN, NUM_MFCC, dtype = float )
        Pile des matrices correspondant au découpage des MFCCs en 
        échantillons de taille
    labels: torch.tensor( ? )
        Étiquette attribuée à chaque échantillon de la pile
    to_id: { int: str }
        Dictionnaire de correspondance indice - nom_audio
    audio_markers: list( int )
        Liste des indices correspondant à la fenêtre d'un nouvel audio
    '''
    DATA_DIR            = None
    MFCC_LEN            = None # Correspond à un enregistrement de 10 secondes
    NUM_MFCC            = None
    NUM_FRAME_SAMPLE    = None
    N_FFT               = 320.0
    MFCC_PARAMS         = None
    OUT_SIZE            = 1


    def __init__(
        self,
        dtype,
        metadata,
        data_dir = None,
        mfcc_len = None,
        num_mfcc = None,
        num_frame_sample = None,
    ):
        '''
        Instanciation de la classe selon une liste de métadonnées constituant 
        le corpus.

        Paramètres
        ----------
        data_dir: str
            Chemin vers le dossier contenant les données
        metadata: list({
            "audio_file_path": str,
            "duration": float,
            "id": str,
            "is_hotword": int (0|1),
            "worker_id": str
        })
            Liste des métadonnées composant le corpus
        '''
        if Dataset.DATA_DIR is None and data_dir is not None:
            Dataset.DATA_DIR = data_dir
        elif Dataset.DATA_DIR is None and data_dir is None:
            Logger.error('Dataset.DATA_DIR needs to be set at least once...')
        if Dataset.MFCC_LEN is None and mfcc_len is not None: 
            Dataset.MFCC_LEN = mfcc_len
        elif Dataset.MFCC_LEN is None and mfcc_len is None:
            Logger.error('Dataset.MFCC_LEN needs to be set at least once...')
        if Dataset.NUM_MFCC is None and num_mfcc is not None:
            Dataset.NUM_MFCC = num_mfcc
        elif Dataset.NUM_MFCC is None and num_mfcc is None:
            Logger.error('Dataset.NUM_MFCC needs to be set at least once...')
        if Dataset.NUM_FRAME_SAMPLE is None and num_frame_sample is not None:
            Dataset.NUM_FRAME_SAMPLE = num_frame_sample
        elif Dataset.NUM_FRAME_SAMPLE is None and num_frame_sample is None:
            Logger.error('Dataset.NUM_FRAME_SAMPLE needs to be set at least once...')
        
        self.metadata = metadata
        ( 
            self.inputs, 
            self.labels, 
            self.to_id
        ) = Dataset.to_dataset(dtype, metadata)


    @staticmethod
    def to_dataset(dtype, metadata):
        '''
        Récupère l'ensemble des MFCCs formattées associées au corpus de données

        Paramètres
        ----------
        dtype: str
            Type de corpus à traiter
        metadata: list({
            "audio_file_path": str,
            "duration": float,
            "id": str,
            "is_hotword": int (0|1),
            "worker_id": str
        })
            Liste des métadonnées composant le corpus

        Retourne
        ----------
        (tensor(float), tensor(int), dict( int: str ))
            Triplet contenant:
                - le tenseur des MFCCs du corpus formattées et concaténées.
                - le tenseur des étiquettes associées
                - un dictionnaire référençant pour chaque id de mfcc, le nom du
                fichier audio correspondant
        '''
        verbose = Logger.VERBOSE
        Logger.VERBOSE = True
        
        # Enregistre les MFCCs extraites des fichiers audio si ce n'est pas 
        # déjà le cas.
        mfcc_fname = f'{Dataset.DATA_DIR}mfcc/{dtype}-{Dataset.NUM_MFCC}.dat'
        if not os.path.isfile(mfcc_fname):
            Dataset.register_mfcc(dtype, metadata)
        
        Logger.wait(f'Extracting MFCCs from {mfcc_fname}')
        mfcc_file = open(mfcc_fname, 'rb')
        mfccs = pickle.load(mfcc_file)
        
        inputs = []
        labels = []
        to_id = {}
        audio_markers = []
        chunk_id = 0
        num_audio = len(metadata)
        Logger.subinfo(f'Number of audio sample: {num_audio}')
        
        frame_length = lambda sample_rate: int(
            Dataset.N_FFT / sample_rate * 1000.0
        )
        
        for md_id, data in enumerate(metadata):
            if data['duration'] > 0.0:
                progress = float(md_id) / float(num_audio - 1)
                print(
                    f'   {int(progress * 50.0)*"•":·<50}{" Done!" if progress == 1.0 else ""}',
                    end = "\r", 
                    flush = True
                )
                
                # On formate la MFCC pour que celle-ci puisse être redécoupée 
                # en une série de matrices de taille `MFCC_LENGTHxNUM_MFCC`.
                audio_markers += [chunk_id]
                for chunk in mfccs[data['audio_file_path']].split(Dataset.MFCC_LEN):
                    inputs.append(Dataset.fit(chunk))
                    label = torch.tensor(data['is_hotword'], dtype = float)
                    labels += [ label ]
                    to_id[chunk_id] = data['audio_file_path']
                    chunk_id += 1
        
        print()
        Logger.VERBOSE = verbose
        
        return (torch.stack(inputs), torch.stack(labels), to_id)

    
    @staticmethod
    def register_mfcc(dtype, metadata):
        '''
        Génère un fichier contenant toutes les MFCCs brutes extraites des 
        fichiers audio contenus dans le corpus. Cette astuce est utile pour
        pouvoir charger plus rapidement les données d'un jeu.

        Paramètres
        ----------
        dtype: str
            Type de jeu de données ({ 'train', 'test', 'dev' })
        metadata: list({
            "audio_file_path": str,
            "duration": float,
            "id": str,
            "is_hotword": int (0|1),
            "worker_id": str
        })
            Liste des métadonnées composant le corpus
        '''
        verbose = Logger.VERBOSE
        Logger.VERBOSE = True

        mfcc_fname = f'{Dataset.DATA_DIR}mfcc/{dtype}-{Dataset.NUM_MFCC}.dat'
        Logger.info(f'Registering {dtype} MFCCs into {mfcc_fname}')
        
        mfccs = {}
        
        num_audio = len(metadata)
        frame_length = lambda sample_rate: int(
            Dataset.N_FFT / sample_rate * 1000.0
        )

        Logger.wait(f'Extracting MFCCs')
        for md_id, data in enumerate(metadata):
            if data['duration'] > 0.0:
                fname = Dataset.DATA_DIR + data['audio_file_path']
                progress = float(md_id) / float(num_audio - 1)
                print(
                    f"   {int(progress * 30.0)*'•':·<30} {fname}…",
                    end = "\r", 
                    flush = True
                )

                # Récupération de la forme d'onde du signal ainsi que sa 
                # fréquence d'échantillonage
                waveform, sample_rate = torchaudio.load(fname)
                if Dataset.MFCC_PARAMS is None:
                    Dataset.MFCC_PARAMS = {
                        'channel': 0,
                        'dither': 0.0,
                        'window_type': "hanning",
                        'frame_length': frame_length(sample_rate),
                        'frame_shift': frame_length(sample_rate) / 2,
                        'remove_dc_offset': False,
                        'round_to_power_of_two': False,
                        'sample_frequency': sample_rate,
                        'num_ceps': Dataset.NUM_MFCC,
                        'num_mel_bins': 256,
                        'snip_edges': False,
                    }
                
                # Transformation de la forme en MFCC
                mfccs[data['audio_file_path']] = Dataset.norm(torchaudio.compliance.kaldi.mfcc(
                    waveform, 
                    **Dataset.MFCC_PARAMS
                ))
        
        print()
        
        Logger.wait(f'Saving MFCCs to {mfcc_fname}')
        mfcc_file = open(mfcc_fname, 'wb')
        pickle.dump(mfccs, mfcc_file)
        mfcc_file.close()
        Logger.success('Done!')

        Logger.VERBOSE = verbose
        
        
    @staticmethod
    def fit(chunk):
        '''
        Formate un tenseur pour homogénéiser les dimensions des entrées 
        composant le jeu de données.

        Paramètres
        ----------
        chunk: torch.tensor
            Tenseur à formatté
        
        Retourne
        --------
        Tenseur formaté (avec zero-padding si nécessaire)
        '''
        return (
            torch.nn.functional.pad(
                chunk, 
                pad = (0, 0, 0, Dataset.MFCC_LEN - chunk.shape[0]),
                mode = 'constant',
                value = 0.0
            ) if chunk.shape[0] % Dataset.MFCC_LEN != 0 else chunk
        ).view(Dataset.NUM_FRAME_SAMPLE, -1) # Sous-découpage de la matrice en fenêtres de 1s
  

    @staticmethod
    def norm(t):
        '''
        Réalise une normalisation centrée réduite d'un tenseur de flottants
        
        Paramètres
        ----------
        t: torch.tensor
            Matrices de données à normaliser
        
        Retourne
        ----------
        torch.tensor
            Matrices centrée réduite
        '''
        return (t - t.mean()) / t.std()
                

    def __len__( self ):
        '''
        Fonction intrinsèque à la classe retournant le nombre total d'entrées la composant

        Retourne
        ----------
        int
            Nombre d'individus composant le jeu de données
        '''
        return len( self.labels )
    

    def __getitem__( self, index ):
        '''
        Récupère un élèment du jeu de données selon son indice

        Paramètre
        ----------
        index: int
            Indice du couple ( fenetre, étiquette ) à extraire
        
        Retourne
        ----------
        ( int, torch.tensor( MFCC_LENGTH, NUM_MFCC ), float )
            Le triplet ( id_découpage, fenetre, étiquette ) associé à l'indice fournit en 
            paramètre.
        '''
        return index, self.inputs[ index ], self.labels[ index ]
